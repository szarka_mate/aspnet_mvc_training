﻿INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Starter'
           ,'Saláta csirkemellel, uborkával, pirított kenyérkockával'
           ,'Cézár saláta'
           ,700
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Starter'
           ,'Amerikai káposztasaláta'
           ,'Coleslaw'
           ,500
           ,0
           ,0)
GO

INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Starter'
           ,'Tökmag pestoval'
           ,'Tócsni'
           ,750
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Soup'
           ,'Húsleves'
           ,'Csigatésztával vagy májgaluskával'
           ,800
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Soup'
           ,'Bableves'
           ,'Tejfölös csülkös'
           ,1000
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Soup'
           ,'Gyümölcsleves'
           ,'Tejszínhabbal, erdei gyümölcsökkel'
           ,1000
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Soup'
           ,'Halászlé'
           ,'Pontyból, harcsából és busából'
           ,1500
           ,1
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('MainDish'
           ,'Pacalpörkölt'
           ,'Törtburgonyával'
           ,1200
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('MainDish'
           ,'Pacalpörkölt'
           ,'Törtburgonyával'
           ,1200
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('MainDish'
           ,'Bécsi szelet'
           ,''
           ,2500
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('MainDish'
           ,'Rántott cukkini'
           ,'Rizzsel, vagy hasábburgonyával'
           ,1500
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('MainDish'
           ,'Marhapörkölt'
           ,'Galuskával'
           ,2500
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Pizza'
           ,'Pizza Margherita'
           ,'Paradicsomszósz, bazsalikom'
           ,1000
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Pizza'
           ,'Pizza Tonno'
           ,'Tonhalas, paradicsomos, mozzarellás'
           ,1500
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Pizza'
           ,'Pizza Quattro Formaggi'
           ,'Négyféle sajttal'
           ,2000
           ,0
           ,1)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Pizza'
           ,'Húsimádó Pizza'
           ,'Sonkával, pikáns szalámival, füstölt kolbásszal'
           ,2000
           ,1
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Dessert'
           ,'Somlói galuska'
           ,''
           ,1000
           ,0
           ,0)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Drink'
           ,'Coca-Cola'
           ,null
           ,300
           ,null
           ,null)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Drink'
           ,'Ásványvíz'
           ,null
           ,300
           ,null
           ,null)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Drink'
           ,'Házi limonádé'
           ,null
           ,650
           ,null
           ,null)
GO
INSERT INTO [dbo].[MenuItems]
           ([Category]
           ,[Description]
           ,[Name]
           ,[Price]
           ,[Spicy]
           ,[Vegetarian])
     VALUES
           ('Drink'
           ,'Red Bull'
           ,null
           ,700
           ,null
           ,null)
GO