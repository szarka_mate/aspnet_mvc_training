﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data
{
    public class AdminRole : IdentityRole<string>
    {
        public AdminRole()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public AdminRole(string name) : this()
        {
            this.Name = name;
        }
    }
}
