﻿using Microsoft.AspNetCore.Identity;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data
{
    public class Costumer : IdentityUser
    {
        [PersonalData]
        [Required]
        [MaxLength(length:255)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [PersonalData]
        [Required]
        [MaxLength(length:255)]
        public string Address { get; set; }
    }
}
