﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data;
using SomeCreativeName_FoodDeliveryComp.DAL;

[assembly: HostingStartup(typeof(SomeCreativeName_FoodDeliveryComp.Areas.Identity.IdentityHostingStartup))]
namespace SomeCreativeName_FoodDeliveryComp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            //builder.ConfigureServices((context, services) => {
            //    services.AddDbContext<CompanyContext>(options =>
            //    options.UseSqlServer(context.Configuration.GetConnectionString("DefaultConnection")));

            //    services.AddDefaultIdentity<Costumer>().AddEntityFrameworkStores<CompanyContext>();
            //});
        }
    }
}