﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using SomeCreativeName_FoodDeliveryComp.DAL;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using SomeCreativeName_FoodDeliveryComp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.Controllers
{

    public class HomeController : Controller
    {
        private readonly CompanyContext _context;
        private readonly IStringLocalizer<HomeController> _localizer;

        public HomeController(CompanyContext context, IStringLocalizer<HomeController> localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.MenuItems.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(int qtty, int id)
        {
            MenuItem _menuItem = await _context.MenuItems.SingleOrDefaultAsync(item => item.MenuItemId == id);

            //routevaluedictionary
            return RedirectToAction("AddToCart", "ShoppingCartItems", new
            {
                _menuItem.MenuItemId,
                _menuItem.Category,
                _menuItem.Description,
                _menuItem.Name,
                _menuItem.Price,
                _menuItem.Spicy,
                _menuItem.Vegetarian,
                _menuItem.OrderCount,
                qtty
                
            });
            
            //int? totalPrice = await _context.ShoppingCartItems.SumAsync(item => item.MenuItemId.Price * item.Quantity);

            //if (menuItem == null || qtty <= 0)
            //{
            //    return RedirectToAction(nameof(Index));
            //}
            //totalPrice = totalPrice == null ? 0 : totalPrice;
            //if (menuItem.Price * qtty + totalPrice <= 20000)
            //{
            //    _context.ShoppingCartItems.Add(new ShoppingCartItem()
            //    {
            //        MenuItemId = menuItem,
            //        Quantity = qtty,
            //    });
            //    await _context.SaveChangesAsync();
            //}


            //return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult GetCategoryMembers(string category)
        {
            return View("CategoryView", _context.MenuItems.Select(m => m).Where(item => item.Category == category).ToList());
        }

        [HttpPost]
        public IActionResult Search(string txt)
        {
            if (txt == null)
                return RedirectToAction(nameof(Index));
            ViewData["searchTerm"] = txt;
            return View("SearchResultView", _context.MenuItems.Select(m => m).Where(item => item.Name.Contains(txt)).ToList());
        }

        public IActionResult About()
        {
            ViewData["Message"] = _localizer["I hope everything works fine."];

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Szarka Máté";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
