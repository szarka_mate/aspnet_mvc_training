﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data;
using SomeCreativeName_FoodDeliveryComp.DAL;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using SomeCreativeName_FoodDeliveryComp.Helpers;
using SomeCreativeName_FoodDeliveryComp.Models;

namespace SomeCreativeName_FoodDeliveryComp.Controllers
{
    public class ShoppingCartItemsController : Controller
    {
        private readonly CompanyContext context;
        private readonly IHtmlLocalizer<ShoppingCartItemsController> localizer;

        public ShoppingCartItemsController(CompanyContext _context, IHtmlLocalizer<ShoppingCartItemsController> _localizer)
        {
            context = _context;
            localizer = _localizer;
        }

        [HttpGet]
        public IActionResult AddToCart(MenuItem menuItem, int qtty)
        {
            SessionObjectNullCheck("cart");

            var cart = SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, "cart");

            CartRepository.AddToCart(cart, menuItem, qtty);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Index()
        {
            SessionObjectNullCheck("cart");
            var cart = SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, "cart");
            return View(CartRepository.GetAll(cart));
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cart = CartRepository.DeleteFromCart(SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, "cart"), (int)id);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            

            return RedirectToAction(nameof(Index));
        }

        public IActionResult EmptyCart()
        {

            CartRepository.EmptyCart(SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, "cart"));
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", new List<ShoppingCartItemViewModel>());
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public IActionResult PlaceOrder()
        {
            SessionObjectNullCheck("cart");
            var cart = SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, "cart");
            if (cart.Count <= 0)
            {
                return RedirectToAction(nameof(Index));
            }

            context.Orders.Add(new Order()
            {
                Costumer = context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name) as Costumer,
                //OrderItemsId = new List<OrderItem>()
            });
            context.SaveChanges();

            var order = new List<OrderItem>();
            foreach (var item in cart)
            {
                context.MenuItems.Update(context.MenuItems.SingleOrDefault(menuitem => menuitem == item.MenuItem));
                order.Add(new OrderItem()
                {
                    MenuItemId = item.MenuItem,
                    Quantity = item.Quantity,
                    Order = context.Orders.Last()
                });

                if (item.MenuItem.OrderCount != null)
                    context.MenuItems.SingleOrDefault(menuitem => menuitem == item.MenuItem).OrderCount += item.Quantity;
                else
                    context.MenuItems.SingleOrDefault(menuitem => menuitem == item.MenuItem).OrderCount = item.Quantity;
            }
            context.Orders.Last().OrderItemsId = order;
            context.SaveChanges();
            EmptyCart();
            //var orderitemsTmp = context.OrderItems.ToList();
            //var orderitems = orderitemsTmp.TakeLast(cart.Count).ToList();

            //context.Orders.Update(context.Orders.Last());
            //context.Orders.Last().OrderItemsId = orderitems;
            context.SaveChanges();
            return RedirectToAction(nameof(Index));


        }


        private void SessionObjectNullCheck(string key)
        {
            var cart = SessionHelper.GetObjectFromJson<List<ShoppingCartItemViewModel>>(HttpContext.Session, key);
            if (cart == null)
            {
                cart = new List<ShoppingCartItemViewModel>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, key, cart);
            }
        }
    }
}
