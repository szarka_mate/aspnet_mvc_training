﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using SomeCreativeName_FoodDeliveryComp.Helpers;
using SomeCreativeName_FoodDeliveryComp.Models;

namespace SomeCreativeName_FoodDeliveryComp.DAL
{
    public static class CartRepository
    {
        public static void AddToCart(List<ShoppingCartItemViewModel> cart, MenuItem menuItem, int quantity)
        {
            if (menuItem !=null && quantity > 0 )
                //ShoppingCart.Sum(item => item.TotalPrice) + menuItem.Price * quantity < 20000)
            {
                ShoppingCartItemViewModel tmp = cart.Select(item => item).FirstOrDefault(id => id.MenuItem.MenuItemId == menuItem.MenuItemId);

                if (tmp != null)
                {
                    tmp.Quantity += quantity;
                }
                else
                {
                    cart.Add(new ShoppingCartItemViewModel()
                    {
                        MenuItem = menuItem,
                        Quantity = quantity
                    });
                }
            }
        }

        public static bool DeleteFromCart(List<ShoppingCartItemViewModel> cart,MenuItem menuItem)
        {
            if (menuItem != null)
            {
                ShoppingCartItemViewModel cartItem = cart.FirstOrDefault(item => item.MenuItem.MenuItemId == menuItem.MenuItemId);

                if (cartItem != null)
                {
                    cart.Remove(cartItem);
                    return true;
                }
            }
            return false;
        }

        public static bool DeleteFromCart(List<ShoppingCartItemViewModel> cart,ShoppingCartItemViewModel cartItem)
        {
            if (cartItem != null && cart.Contains(cartItem))
            {
                cart.Remove(cartItem);
                return true;
            }
            return false;
        }

        public static List<ShoppingCartItemViewModel> DeleteFromCart(List<ShoppingCartItemViewModel> cart, int idx)
        {
            cart.Remove(cart.FirstOrDefault(item => item.MenuItem.MenuItemId == idx));
            return cart;
        }

        public static void EmptyCart(List<ShoppingCartItemViewModel> cart)
        {
            cart.Clear();
        }

        public static IEnumerable<ShoppingCartItemViewModel> GetAll(List<ShoppingCartItemViewModel> cart)
        {
            return cart;
        }
    }
}
