﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.DAL
{
    public class CompanyContext : IdentityDbContext<Costumer, IdentityRole, string>
    {
        public CompanyContext(DbContextOptions<CompanyContext> options) : base(options)
        {
        }

        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<OrderItem>().HasOne(o => o.Order).WithMany(oi => oi.OrderItemsId).OnDelete(DeleteBehavior.Cascade);

            //var admin = new Costumer { Id = "0", Email = "admin@admin.com", UserName = "admin@admin.com", PasswordHash = "admin" };
            //var adminRole = new IdentityRole { Id = "0", Name = "admin" };

            //var role = new IdentityUserRole<string>
            //{
            //    UserId = admin.Id,
            //    RoleId = adminRole.Id
            //};

            //builder.Entity<IdentityUserRole<string>>().HasData(role);
            base.OnModelCreating(builder);
        }
    }
}
