﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.DAL.DataModels
{
    public class MenuItem
    {
        [Key]
        //[Index(IsUnique = true)]
        public int MenuItemId { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int? Price { get; set; }
        public bool? Spicy { get; set; }
        public bool? Vegetarian { get; set; }
        public int? OrderCount { get; set; }
    }
}
