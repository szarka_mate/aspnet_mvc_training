﻿using SomeCreativeName_FoodDeliveryComp.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.DAL.DataModels
{
    public class Order
    {
        [Key, Column(Order = 0)]
        public int OrderId { get; set; }

        public ICollection<OrderItem> OrderItemsId { get; set; }

        [ForeignKey("Id")]
        public Costumer Costumer { get; set; }
    }
}
