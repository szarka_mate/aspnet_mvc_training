﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.DAL.DataModels
{
    public class OrderItem
    {
        [Key]
        //[Index(IsUnique = true)]
        public int OrderItemId { get; set; }
        [ForeignKey("MenuItems")]
        public MenuItem MenuItemId { get; set; }
        public int Quantity { get; set; }
        public Order Order { get; set; }
    }
}
