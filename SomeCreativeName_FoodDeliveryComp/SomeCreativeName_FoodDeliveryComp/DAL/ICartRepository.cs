﻿using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;
using SomeCreativeName_FoodDeliveryComp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeCreativeName_FoodDeliveryComp.DAL
{
    public interface ICartRepository
    {
        void AddToCart(List<ShoppingCartItemViewModel> cart, MenuItem menuItem, int quantity);
        bool DeleteFromCart(List<ShoppingCartItemViewModel> cart, MenuItem menuItem);
        bool DeleteFromCart(List<ShoppingCartItemViewModel> cart, ShoppingCartItemViewModel cartItem);
        bool DeleteFromCart(List<ShoppingCartItemViewModel> cart, int idx);
        void EmptyCart(List<ShoppingCartItemViewModel> cart);

        IEnumerable<ShoppingCartItemViewModel> GetAll(List<ShoppingCartItemViewModel> cart);
    }
}
