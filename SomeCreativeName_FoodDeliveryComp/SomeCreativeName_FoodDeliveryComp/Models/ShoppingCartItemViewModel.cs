﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SomeCreativeName_FoodDeliveryComp.DAL.DataModels;

namespace SomeCreativeName_FoodDeliveryComp.Models
{
    public class ShoppingCartItemViewModel
    {
        public MenuItem MenuItem { get; set; }
        public int Quantity { get; set; }
        public int TotalPrice
        {
            get
            {
                return (int)MenuItem.Price * Quantity;
            }
        }
    }
}
