#pragma checksum "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "60096b035290f75a5ae740bb66e648a0e1b97a2b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_SearchResultView), @"mvc.1.0.view", @"/Views/Home/SearchResultView.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/SearchResultView.cshtml", typeof(AspNetCore.Views_Home_SearchResultView))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\_ViewImports.cshtml"
using SomeCreativeName_FoodDeliveryComp;

#line default
#line hidden
#line 2 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\_ViewImports.cshtml"
using SomeCreativeName_FoodDeliveryComp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"60096b035290f75a5ae740bb66e648a0e1b97a2b", @"/Views/Home/SearchResultView.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"39f1cc2686557ee3e9474d4afe70700fba5e7ebd", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_SearchResultView : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<SomeCreativeName_FoodDeliveryComp.DAL.DataModels.MenuItem>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Search", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/food_image.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("img-responsive"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddToCart", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
  
    ViewData["Title"] = "Search results";

#line default
#line hidden
            BeginContext(129, 160, true);
            WriteLiteral("\r\n\r\n<div class=\"container body-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"form-group col-xs-12 col-sm-6 col-md-3 col-lg-3\">\r\n            ");
            EndContext();
            BeginContext(289, 345, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "df1ca7207ef84a75bd4cc4c19fd1139d", async() => {
                BeginContext(351, 96, true);
                WriteLiteral("\r\n                <label>Filter</label>\r\n                <input type=\"text\" class=\"form-control\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 447, "\"", 478, 1);
#line 12 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
WriteAttributeValue("", 455, ViewData["searchTerm"], 455, 23, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(479, 148, true);
                WriteLiteral(" onkeyup=\"$(\'#search\').click()\" name=\"txt\" id=\"fltr\" />\r\n                <div class=\"hidden\"><input type=\"submit\" id=\"search\" /></div>\r\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(634, 139, true);
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"container-fluid\">\r\n        <section id=\"favs\">\r\n            <div class=\"row text-center\">\r\n");
            EndContext();
#line 21 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                 foreach (var item in Model)
                {

#line default
#line hidden
            BeginContext(838, 109, true);
            WriteLiteral("                    <div class=\" col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center\">\r\n                        ");
            EndContext();
            BeginContext(947, 60, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "ad51f681407d452bb9573357bb2cc7fc", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1007, 30, true);
            WriteLiteral("\r\n                        <h3>");
            EndContext();
            BeginContext(1038, 39, false);
#line 25 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                       Write(Html.DisplayFor(modelItem => item.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1077, 35, true);
            WriteLiteral("</h3>\r\n                        <h5>");
            EndContext();
            BeginContext(1113, 46, false);
#line 26 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                       Write(Html.DisplayFor(modelItem => item.Description));

#line default
#line hidden
            EndContext();
            BeginContext(1159, 35, true);
            WriteLiteral(" </h5>\r\n                        <p>");
            EndContext();
            BeginContext(1195, 40, false);
#line 27 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                      Write(Html.DisplayFor(modelItem => item.Price));

#line default
#line hidden
            EndContext();
            BeginContext(1235, 6, true);
            WriteLiteral("</p>\r\n");
            EndContext();
#line 28 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                         if (item.Spicy == true)
                        {

#line default
#line hidden
            BeginContext(1318, 90, true);
            WriteLiteral("                            <span class=\"glyphicon glyphicon-fire\" title=\"Spicy\"></span>\r\n");
            EndContext();
#line 31 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                        }

#line default
#line hidden
            BeginContext(1435, 24, true);
            WriteLiteral("                        ");
            EndContext();
#line 32 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                         if (item.Vegetarian == true)
                        {

#line default
#line hidden
            BeginContext(1517, 105, true);
            WriteLiteral("                            <span class=\"glyphicon glyphicon-tree-deciduous\" title=\"Vegetarian\"></span>\r\n");
            EndContext();
#line 35 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                        }

#line default
#line hidden
            BeginContext(1649, 56, true);
            WriteLiteral("                        <br />\r\n                        ");
            EndContext();
            BeginContext(1705, 344, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a883ee1a63bf4e2380698b9a88722b7c", async() => {
                BeginContext(1756, 125, true);
                WriteLiteral("\r\n                            <input type=\"number\" value=\"1\" name=\"qtty\" />\r\n                            <input type=\"hidden\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 1881, "\"", 1905, 1);
#line 39 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
WriteAttributeValue("", 1889, item.MenuItemId, 1889, 16, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1906, 136, true);
                WriteLiteral(" name=\"id\" />\r\n                            <input type=\"submit\" class=\"btn btn-primary\" value=\"Add to cart\" />\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2049, 30, true);
            WriteLiteral("\r\n                    </div>\r\n");
            EndContext();
#line 43 "C:\Users\mates\Desktop\aspnet_mvc_training\SomeCreativeName_FoodDeliveryComp\SomeCreativeName_FoodDeliveryComp\Views\Home\SearchResultView.cshtml"
                }

#line default
#line hidden
            BeginContext(2098, 169, true);
            WriteLiteral("            </div>\r\n        </section>\r\n    </div>\r\n    <hr />\r\n    <footer>\r\n        <p>&copy; 2018 - SomeCreativeName_FoodDeliveryComp</p>\r\n    </footer>\r\n</div>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<SomeCreativeName_FoodDeliveryComp.DAL.DataModels.MenuItem>> Html { get; private set; }
    }
}
#pragma warning restore 1591
