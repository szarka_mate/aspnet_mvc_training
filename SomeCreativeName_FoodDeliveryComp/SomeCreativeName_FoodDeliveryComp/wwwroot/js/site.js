﻿$(document).ready(function () {
    var input = $("#fltr");
    var len = input.val().length;
    if (!(input[0] === document.activeElement)) {
        input[0].focus();
        input[0].setSelectionRange(len, len);
    }
});